Router.configure({
    layoutTemplate: 'layout'
});

Router.map(function() {
    this.route('index', {
	path : "/"
    });
    this.route('register');
    this.route('login');
    this.route('logout');
    this.route('homeFeed', {
	path : "/feed"
    });
    this.route('feed', {
	path: '/feed=:_feedID',
	data: function() {
	    var res = Meteor.users.find({username: this.params._feedID}).fetch();
	    if(res.length <= 0){
		return false;
	    }
            return {username: this.params._feedID, profile: res[0].profile};
        },
	waitOn: Meteor.subscribe("allUsers"),
	notFoundTemplate: 'userNotFound'
    });
    this.route('friendlist');
    this.route('search', {
	path: '/search=:_searchID',
	data: function() {
            return {search: this.params._searchID};
        }
    });
    this.route('*', {
	template: "404"
    });
});

//Subscriptions
Meteor.subscribe("allMsgs");
Meteor.subscribe("allUsers");

//Functions
function getMsgList(username){
    var res = [];
    var msglist = FeedMsgs.find({to: username}, {sort: [["time", "desc"], ["from", "desc"]]}).fetch();
    for(var i in msglist){
	res[i] = Meteor.users.find({username: msglist[i].from}).fetch();
	res[i] = res[i][0].profile;
	res[i]["from"] = msglist[i].from;
	res[i]["message"] = msglist[i].msg;
    }
    return res;
}

//Helpers

Template.layout.events({
    'click #logoutLink': function(event) {
	Meteor.logout();
    },
    'submit #searchForm': function(event) {
	event.preventDefault();
	var searchText = $("#searchText").val();
        $("#searchText").val("");
	Router.go('/search='+searchText);
    }
});

Template.layout.friendRequests = function(){
    if(Meteor.user() === null || Meteor.user() === undefined)
	return;
    var res = Meteor.user().profile.pending.length;

    return (res <= 0) ? "" : res;
}

Template.register.events({
    'submit #registerform': function(event) {
	event.preventDefault();
	var username = $("#username").val();
	var realname = $("#realName").val();
	var location = $("#location").val();
	var password = $("#password").val();
	var passwordAgain = $("#password_again").val();

	if(password !== passwordAgain){
	    $("#errorMsg").find("span").text("Passwords must match!").parent().slideDown(800);
	    return;
	}

	var options = { username: username, password: password,
			profile: {name: realname, location: location,
				  aboutMe: "Write something about yourself.",
				  friends: [], pending: []}};

	Accounts.createUser(options, function(e){
	    if(e !== undefined){
		$("#errorMsg").find("span").text(e.reason).parent().slideDown(800);
	    }else{
		Router.go("/feed");
	    }
	});
    }
});

Template.login.events({
    'submit #loginform': function(event) {
	event.preventDefault();
	var username = $("#loginUsername").val();
	var password = $("#loginPassword").val();
	Meteor.loginWithPassword(username, password, function(e){
	    if(e !== undefined){
		$("#errorMsg").find("span").text(e.reason).parent().slideDown(800);
	    } else {
		Router.go("/feed");
	    }
	});
    }
});

Template.homeFeed.events({
    'submit #msgForm': function(event) {
	event.preventDefault();
	var user = Meteor.user().username;
	var msg = $("#userMsg").val();
	$("#userMsg").val("");
	FeedMsgs.insert({from: user, to: user, msg: msg, time: Date.now()}, function(e, msgID){
	    if(e !== undefined){
		$("#errorMsg").find("span").text(e.reason).parent().slideDown(800);
	    } else {
		$("#errorMsg").slideUp(800);
	    }
	});
    }
});

Template.homeFeed.messageList = function(){
    if(Meteor.user() === undefined || Meteor.user() === null)
	return;
    return getMsgList(Meteor.user().username);
}

Template.feed.messageList = function(){
    if(this.username === "")
	return;
    return getMsgList(this.username);
}

Template.feed.isFriends = function(){
    if(Meteor.user() === null || Meteor.user() === undefined || this.username === undefined)
	return;
    var res = Meteor.user().profile.friends;
    if(res.length <= 0) return false;
    if($.inArray(this.username, res) == -1){
    	return false;
    } else {
    	return true;
    }
}

Template.homeFeed.rendered = function(){
    $(this.find('#aboutMe.editable:not(.editable-click)')).editable('destroy').editable({
	success: function(response, newValue) {
	    Meteor.call("updateUserProfile", "profile.aboutMe", newValue);
	}});
    $(this.find('#name.editable:not(.editable-click)')).editable('destroy').editable({
	success: function(response, newValue) {
	    Meteor.call("updateUserProfile", "profile.name", newValue);
	}});
    $(this.find('#location.editable:not(.editable-click)')).editable('destroy').editable({
	success: function(response, newValue) {
	    Meteor.call("updateUserProfile", "profile.location", newValue);
	}});
}

Template.feed.events({
    'submit #msgForm': function(event) {
	event.preventDefault();
	var from = Meteor.user().username;
	var to = this.username;
	var msg = $("#userMsg").val();
	$("#userMsg").val("");
	FeedMsgs.insert({from: from, to: to, msg: msg, time: Date.now()}, function(e, msgID){
	    if(e !== undefined){
		$("#errorMsg").find("span").text(e.reason).parent().slideDown(800);
	    } else {
		$("#errorMsg").slideUp(800);
	    }
	});
    },
    'click #friendRequest': function(event){
	event.preventDefault();
	Meteor.call("friendRequest", Meteor.user().username, this.username);
    },
    'click #acceptFriendRequest': function(event){
	event.preventDefault();
	Meteor.call("acceptFriendRequest", Meteor.user().username, this.username);
    },
    'click #denyFriendRequest': function(event){
	event.preventDefault();
	Meteor.call("denyFriendRequest", Meteor.user().username, this.username);
    },
    'click #unfriend': function(event){
	event.preventDefault();
	Meteor.call("friendRemove", Meteor.user().username, this.username);
    }
});

Template.feed.friendStateButton = function(){
    if(Meteor.user() === null || Meteor.user() === undefined || this.username === undefined)
	return;
    var ownFriends = Meteor.user().profile;
    var otherFriends = Meteor.users.find({username: this.username}).fetch();

    if($.inArray(Meteor.user().username, otherFriends[0].profile.pending) != -1){
	return '<button class="btn btn-info btn-sm" id="friendRequestSent">Friend request sent</button>';
    }else if($.inArray(this.username, ownFriends.pending) != -1){
    	return '<button class="btn btn-success btn-sm" id="acceptFriendRequest">Accept friend request</button>'+
	    '<button class="btn btn-danger btn-sm" id="denyFriendRequest">Deny friend request</button>';
    }else if($.inArray(this.username, ownFriends.friends) == -1){
    	return '<button class="btn btn-success btn-sm" id="friendRequest">Send friend request</button>';
    }else{
	return '<button class="btn btn-danger btn-sm" id="unfriend">Unfriend</button>';
    }
}

Template.errorMsg.events({
    "click .close": function(event){
	$("#errorMsg").slideUp(800);
    }
});

Template.friendlist.events({
    'click #acceptFriendRequest': function(event){
	event.preventDefault();
	var accept = event.currentTarget;
	Meteor.call("acceptFriendRequest", Meteor.user().username, $(accept).parent().attr("name"));
    },
    'click #denyFriendRequest': function(event){
	event.preventDefault();
	var deny = event.currentTarget;
	Meteor.call("denyFriendRequest", Meteor.user().username, $(deny).parent().attr("name"));
    }
});

Template.friendlist.pendingRequests = function(){
    if(Meteor.user() === undefined || Meteor.user() === null)
	return;
    var res = Meteor.user().profile.pending.length;

    return (res <= 0) ? false : true;
}

Template.friendlist.requestList = function(){
    if(Meteor.user() === undefined || Meteor.user() === null)
	return;
    var res = [];
    var pendingUsernames = Meteor.user().profile.pending;
    for(var i in pendingUsernames){
	res[i] = Meteor.users.find({username: pendingUsernames[i]}).fetch();
	res[i] = res[i][0];
    }
    return res;
}

Template.friendlist.friends = function(){
    if(Meteor.user() === undefined || Meteor.user() === null)
	return;
    var res = [];
    var friendUsernames = Meteor.user().profile.friends;
    for(var i in friendUsernames){
	res[i] = Meteor.users.find({username: friendUsernames[i]}).fetch();
	res[i] = res[i][0];
    }
    return res;
}

Template.search.results = function(){
    var searchRegex = new RegExp('.*'+this.search+'.*', 'i');
    return Meteor.users.find({$or: [{"profile.name": searchRegex}, {"profile.location": searchRegex}]});
}
