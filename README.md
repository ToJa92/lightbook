# Lightbook

A simple social network created as part of the course [TDP013](https://www.ida.liu.se/~TDP013/).

# Requirements

* [Meteor](https://www.meteor.com)
* [Meteorite](https://github.com/oortcloud/meteorite)

# Getting started

```
$ mrt update
$ meteor add iron-router bootstrap-3 x-editable-bootstrap
$ meteor run
```