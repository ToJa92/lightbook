var assert = require('assert');

suite('User Authorization', function() {

    test('Update user profile field', function(done, server, client) {
	client.eval(function() {
	    var options = { username: "aa", password: "aa", 
			    profile: {name: "aa", friends: [], pending: []}};
	    Accounts.createUser(options);
	    Meteor.logout();
	});

	server.eval(observeCollection);

	function observeCollection() {
	    Meteor.users.find({username: "aa"}).observe({
		changed: function(newDoc, oldDoc) {
		    if(newDoc.profile.name == oldDoc.profile.name)
			return;
		    emit('changed', newDoc, oldDoc);
		}
	    });
	}	

	server.once('changed', function(newDoc, oldDoc) {
	    assert.equal(newDoc.profile.name, "new name");
	    done();
	});

	client.eval(function() {
	    Meteor.loginWithPassword("aa", "aa", function(){
		Meteor.call("updateUserProfile", "profile.name", "new name");
	    });
	});
    });
});

suite("User interaction", function(){
    test("Become friends", function(done, server, client){
	client.eval(function() {
	    var options = { username: "aa", password: "aa", 
			    profile: {name: "aa", friends: [], pending: []}};
	    Accounts.createUser(options);
	    var options = { username: "bb", password: "bb", 
			    profile: {name: "bb", friends: [], pending: []}};
	    Accounts.createUser(options);
	    Meteor.logout();
	});

	client.once("testrequest", function(user){

	    assert.equal(user.profile.pending[0], "bb");
	    done();
	});

	client.eval(function() {
	    Meteor.loginWithPassword("bb", "bb", function(){
				
		Meteor.call("friendRequest", "bb", "aa", function(){
		    var aa = Meteor.users.find({username: "aa"}).fetch();	
		    emit("testrequest", aa[0]);
		});
	    });
	});
    });
    test("Accept friend", function(done, server, client){
	client.eval(function() {
	    var options = { username: "aa", password: "aa", 
			    profile: {name: "aa", friends: [], pending: []}};
	    Accounts.createUser(options);
	    var options = { username: "bb", password: "bb", 
			    profile: {name: "bb", friends: [], pending: []}};
	    Accounts.createUser(options);
	    Meteor.logout();
	});

	client.once("acceptrequest", function(user){
	    assert.equal(user.profile.friends[0], "bb");
	    assert.equal(user.profile.pending[0], undefined);
	    done();
	});

	client.eval(function() {
	    Meteor.loginWithPassword("bb", "bb", function(){
				
		Meteor.call("friendRequest", "bb", "aa", function(){
		    Meteor.logout();
		    Meteor.loginWithPassword("aa", "aa", function(){
			Meteor.call("acceptFriendRequest", "aa", "bb", function(){
			    var aa = Meteor.users.find({username: "aa"}).fetch();	
			    emit("acceptrequest", aa[0]);
			});
		    });
		});
	    });
	});
    });
   test("Deny friend", function(done, server, client){
	client.eval(function() {
	    var options = { username: "aa", password: "aa", 
			    profile: {name: "aa", friends: [], pending: []}};
	    Accounts.createUser(options);
	    var options = { username: "bb", password: "bb", 
			    profile: {name: "bb", friends: [], pending: []}};
	    Accounts.createUser(options);
	    Meteor.logout();
	});

	client.once("denyrequest", function(user){
	    assert.equal(user.profile.friends[0], undefined);
	    assert.equal(user.profile.pending[0], undefined);
	    done();
	});

	client.eval(function() {
	    Meteor.loginWithPassword("bb", "bb", function(){
				
		Meteor.call("friendRequest", "bb", "aa", function(){
		    Meteor.logout();
		    Meteor.loginWithPassword("aa", "aa", function(){
			Meteor.call("denyFriendRequest", "aa", "bb", function(){
			    var aa = Meteor.users.find({username: "aa"}).fetch();	
			    emit("denyrequest", aa[0]);
			});
		    });
		});
	    });
	});
    });
   test("Accept friend, remove friend", function(done, server, client){
	client.eval(function() {
	    var options = { username: "aa", password: "aa", 
			    profile: {name: "aa", friends: [], pending: []}};
	    Accounts.createUser(options);
	    var options = { username: "bb", password: "bb", 
			    profile: {name: "bb", friends: [], pending: []}};
	    Accounts.createUser(options);
	    Meteor.logout();
	});

	client.once("friendRemoved", function(user, user2){
	    assert.equal(user.profile.friends[0], undefined);
	    assert.equal(user2.profile.friends[0], undefined);
	    done();
	});

	client.eval(function() {
	    Meteor.loginWithPassword("bb", "bb", function(){
				
		Meteor.call("friendRequest", "bb", "aa", function(){
		    Meteor.logout();
		    Meteor.loginWithPassword("aa", "aa", function(){
			Meteor.call("acceptFriendRequest", "aa", "bb", function(){
			    Meteor.call("friendRemove", "aa", "bb", function(){
				var aa = Meteor.users.find({username: "aa"}).fetch();
				var bb = Meteor.users.find({username: "bb"}).fetch();
				emit("friendRemoved", aa[0], bb[0]);
			    });
			});
		    });
		});
	    });
	});
    });
});
