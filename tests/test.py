from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import os
from random import randint


def logout(browser):
    browser.find_element_by_xpath("//div[@class='btn-group'][1]/button[1]").click()
    browser.find_element_by_id("logoutLink").click()
    time.sleep(0.5)

def login(browser, username):
    browser.get(baseurl+"login")
    browser.find_element_by_id("loginUsername").send_keys(username)
    pw = browser.find_element_by_id("loginPassword")
    pw.send_keys(username)
    pw.submit()
    time.sleep(0.5)

def register(browser, username):
    browser.get(baseurl+"register")
    browser.find_element_by_id("username").send_keys(username)
    browser.find_element_by_id("realName").send_keys(username)
    browser.find_element_by_id("location").send_keys(username)
    browser.find_element_by_id("password").send_keys(username)
    browser.find_element_by_id("password_again").send_keys(username)
    browser.find_element_by_id("password").submit()
    time.sleep(0.5)

def postOnWall(browser, msg):
    feedMsgInput = browser.find_element_by_id("userMsg")
    feedMsgInput.send_keys(msg)
    feedMsgInput.submit()
    time.sleep(0.5)

path = os.path.dirname(os.path.realpath(__file__))
browser = webdriver.Firefox() # Get local session of firefox
baseurl = "http://localhost:3000/"

aa = "a"+str(randint(0,999999999))
bb = "b"+str(randint(0,999999999))

register(browser, aa)

feed = browser.find_element_by_id("userMsg")

msgs = ["A","B","C"]

for i in msgs:
    feed.send_keys(i)
    feed.submit()


msgfeed = browser.find_element_by_id("msgFeed")
msglist = msgfeed.find_elements_by_tag_name("li")

for i,j in zip(reversed(msglist), msgs):
    assert j == i.find_element_by_tag_name("p").text

namebox = browser.find_element_by_id("name")
namebox.click()
time.sleep(0.5)
textarea = browser.find_element_by_class_name("editable-input").find_element_by_class_name("form-control")
textarea.clear()
textarea.send_keys("123")
textarea.submit()
time.sleep(0.5)

assert browser.find_element_by_id("name").text == "123"

logout(browser)
register(browser, bb)

searchField = browser.find_element_by_id("searchText");
searchField.send_keys(aa)
searchField.submit()

searchResultLink = browser.find_element_by_xpath("//ul[@id='searchMatches']/li[1]/h3[1]/a[1]")
assert searchResultLink.text == "123"
searchResultLink.click()

browser.find_element_by_id("friendRequest").click()
logout(browser)

login(browser, aa)
assert browser.find_element_by_id("requestCount").text == "1"
browser.find_element_by_xpath("//div[@class='btn-group'][1]/button[1]").click()
browser.find_element_by_xpath("//ul[@class='dropdown-menu'][1]/li[1]/a[1]").click()
browser.find_element_by_id("acceptFriendRequest").click()
time.sleep(0.5)
friendLink = browser.find_element_by_xpath("//ul[@id='friendList'][1]/li[1]/h3[1]/a[1]")
assert friendLink.text == bb
friendLink.click()

postOnWall(browser, aa)
msgText = browser.find_element_by_xpath("//ul[@id='msgFeed'][1]/li[1]/p[1]")
assert aa == msgText.text

logout(browser)
login(browser, bb)
browser.get(baseurl+"feed="+aa)
time.sleep(0.5)
postOnWall(browser, "hej "+aa)
msgText = browser.find_element_by_xpath("//ul[@id='msgFeed'][1]/li[1]/p[1]")
assert "hej "+aa == msgText.text

browser.find_element_by_id("unfriend").click()
time.sleep(0.5)
assert "Become friends with 123 to see this person's feed." == browser.find_element_by_xpath("//div[@id='container'][1]/p[1]").text

print "Test complete, no errors"

browser.close()
