Meteor.startup(function () {
    Meteor.publish("allMsgs", function() {
    	return FeedMsgs.find();
    });
    Meteor.publish("allUsers", function () {
	return Meteor.users.find();
    });
});

Meteor.methods({
    friendRequest: function(username, newFriend){
	return Meteor.users.update({username: newFriend}, {$push: {"profile.pending": username}});
    },
    acceptFriendRequest: function(username, newFriend){
	Meteor.users.update({username: username}, {$pull: {"profile.pending": newFriend}});
	Meteor.users.update({username: username}, {$push: {"profile.friends": newFriend}});
	return 	Meteor.users.update({username: newFriend}, {$push: {"profile.friends": username}});
    },
    denyFriendRequest: function(username, newFriend){
	return Meteor.users.update({username: username}, {$pull: {"profile.pending": newFriend}});
    },
    friendRemove: function(username, oldFriend){
	Meteor.users.update({username: oldFriend}, {$pull: {"profile.friends": username}});
	return Meteor.users.update({username: username}, {$pull: {"profile.friends": oldFriend}});
    },
    updateUserProfile: function(field, newData){
        var action = {};
        action[field] = newData;
	return Meteor.users.update({username : Meteor.user().username}, {$set: action});
    }
});
